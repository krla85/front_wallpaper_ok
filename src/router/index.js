import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Auth from "../views/Auth.vue";
import ShowWallpaper from "../views/ShowWallpaper.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/Home",
    name: "Home",
    component: Home
  },
  {
    path: "/",
    name: "Auth",
    component: Auth
  },
  {
    path: "/wallpaper/:id",
    name: "Wallpaper",
    component: ShowWallpaper
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

