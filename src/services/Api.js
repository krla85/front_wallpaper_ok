import axios from 'axios'

const API = axios.create({
  baseURL: 'http://localhost:3000/api',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  async signup (newUser) {
    const response = await API.post('/auth/signup', {
      ...newUser
    })
    return response.data
  },
  async login (user) {
    const response = await API.post('/auth/login', {
      ...user
    })
    return response.data
  },
  async getAllWallpapers () {
    const response = await API.get('/wallpapers')
    return response.data
  },
  async getOneWallpaper (wallpaperId) {
    const response = await API.get(`/wallpapers/${wallpaperId}`);
    return response.data
  },
  async getWallpapersbyauthor (author) {
    const response = await API.get(`/wallpapers/author?author=${author}`)
    return response.data
  },
  async getWallpapersBySize (width, height) {
    const response = await API.get(`/wallpapers/size?widthQuery=${width}&heightQuery=${height}`)
    return response.data
  }
}
